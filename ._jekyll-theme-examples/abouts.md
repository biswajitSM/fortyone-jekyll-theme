---
layout: landing
collection: abouts
order: 1

title: Abouts Landing
description: This Pages is using the landing layout showing the collection of abouts
image: "https://source.unsplash.com/random/800x600"
show_excerpts: true
style: 1
---